import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router }             from '@angular/router';
import { PAGES_ARR }                          from '@constants/shared';

@Component({
  selector: 'app-program',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.scss']
})
export class ProgramComponent {

  selectedTab = 0;
  pagesArr = PAGES_ARR;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    const querySelectedTab = +this.activatedRoute.snapshot.queryParams.selectedTab;
    this.selectedTab = querySelectedTab || this.selectedTab;
  }

  onTab(queryParam: number) {
    this.router.navigate(['/program'], {queryParams: {selectedTab: queryParam}});
  }
}
