import { Component }        from '@angular/core';
import { AppConfigService } from '../../../../app-config.service';
import { PROGRAM_TITLE }    from '@constants/shared';

@Component({
  selector: 'app-cultural',
  templateUrl: './cultural.component.html',
  styleUrls: ['./cultural.component.scss']
})
export class CulturalComponent {
  cardBasic = this.appConfigService.config.value['PROGRAM']['BASIC_PROGRAM_TEXT'];
  cardOptional = this.appConfigService.config.value['PROGRAM']['OPTIONAL_PROGRAM_TEXT'];
  program = PROGRAM_TITLE;

  constructor(
    private appConfigService: AppConfigService
  ) {
  }
}
