import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-program-list-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './program-list-card.component.html',
  styleUrls: ['./program-list-card.component.scss']
})
export class ProgramListCardComponent {

  @Input() card: any;
  @Input() isImgRight = false;
  @Input() hasMarginBottom = true;

  isMobile = window.innerWidth <= 576;
}
