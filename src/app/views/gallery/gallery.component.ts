import { ChangeDetectionStrategy, Component, OnInit }              from '@angular/core';
import includes                                                    from 'lodash-es/includes';
import isEmpty                                                     from 'lodash-es/isEmpty';
import { NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions } from 'ngx-gallery';
import { AppConfigService }                                        from '../../app-config.service';

@Component({
  selector: 'app-gallery',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  it = [];
  galleries = [];
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  yearDescription = [];
  imagesArr = this.appConfigService.config.value['GALLERY']['GALLERY_IMAGE_ARR'];
  firstYearDescription = this.appConfigService.config.value['GALLERY']['GALLERY_YEAR_DESCRIPTION'];


  constructor(
    private appConfigService: AppConfigService
  ) {}

  ngOnInit() {
    let lalkaArr = [];
    this.yearDescription = this.firstYearDescription.sort((a, b) => a.year - b.year);
    this.imagesArr.sort((a, b) => a.label - b.label);

    this.imagesArr.forEach(image => {
      image['small'] = image.big;
      image['medium'] = image.big;
      if (isEmpty(lalkaArr) || lalkaArr[lalkaArr.length - 1].label === image.label) {
        lalkaArr = [...lalkaArr, image];
      } else {
        this.galleries = [...this.galleries, lalkaArr];
        lalkaArr = [image];
      }
    });

    this.galleries = [...this.galleries, lalkaArr];

    let yearsArr = [];
    this.imagesArr.forEach(image => {
      if (!includes(yearsArr, image.label)) {
        yearsArr = [...yearsArr, image.label];
      }
    });
    // this.curIndex = yearsArr.length - 1;

    this.it = [];
    yearsArr.forEach(year => {
      const newItItem = {
        label: `${year}`,
      };
      this.it = [...this.it, newItItem];
    });

    this.galleryOptions = [
      {imageDescription: true},
      {
        width: '100%',
        height: '100vh',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      {
        breakpoint: 800,
        width: '100%',
        height: '30vh',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];
    this.galleryImages = this.galleries[this.galleries.length - 1];
  }

  // onIndexChange(index: number): void {
  //   this.curIndex = index;
  // }
}
