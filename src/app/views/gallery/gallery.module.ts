import { NgModule }                         from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import en                                   from '@angular/common/locales/en';
import { RouterModule, Routes }             from '@angular/router';
import { GalleryComponent }                 from '@views/gallery/gallery.component';
import { NgxGalleryModule }                 from 'ngx-gallery';
import { NzIconModule }                     from 'ng-zorro-antd/icon';

registerLocaleData(en);

const VIEWS_COMPONENTS = [GalleryComponent];
const BROWSER_MODULES = [CommonModule];
const ANT_DESIGN_MODULES = [NzIconModule];

const routes: Routes = [
  {path: '', component: GalleryComponent}
];

@NgModule({
  declarations: [VIEWS_COMPONENTS],
  imports: [RouterModule.forChild(routes), BROWSER_MODULES, ANT_DESIGN_MODULES, NgxGalleryModule],
})
export class GalleryModule {
}
