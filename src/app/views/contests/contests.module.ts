import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import { ContestsComponent }        from './contests.component';
import { RouterModule, Routes }     from '@angular/router';
import { FontDisplayNoneDirective } from '@views/contests/set-src.directive';

const VIEWS_COMPONENTS = [ContestsComponent];
const BROWSER_MODULES = [CommonModule];

const routes: Routes = [
  {path: '', component:  ContestsComponent}
];

@NgModule({
  declarations: [VIEWS_COMPONENTS, FontDisplayNoneDirective],
  imports: [BROWSER_MODULES, RouterModule.forChild(routes)],
})
export class ContestsModule { }
