import {Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appSrc]'
})
export class FontDisplayNoneDirective implements OnInit {
  @Input() url;

  constructor(private input: ElementRef) {}

  ngOnInit(): void {
    this.input.nativeElement.setAttribute('src', this.url);
  }
}
