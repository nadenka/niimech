import { Component }              from '@angular/core';
import { AppConfigService }       from '../../app-config.service';

@Component({
  selector: 'app-contests',
  templateUrl: './contests.component.html',
  styleUrls: ['./contests.component.scss']
})
export class ContestsComponent {
  videos = this.appConfigService.config.value['CONTESTS']['VIDEOS_ARR'];
  photos = this.appConfigService.config.value['CONTESTS']['PHOTOS_ARR'];

  constructor(
    private appConfigService: AppConfigService
  ) {
  }
}
