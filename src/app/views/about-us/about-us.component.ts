import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AppConfigService }                   from '../../app-config.service';

@Component({
  selector: 'app-about-us',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent {

  aboutUs = this.appConfigService.config.value['ABOUT_US']['ABOUT_US_PAGE_TEXT'];

  constructor(
    private appConfigService: AppConfigService
  ) {
  }
}
