import { NgModule }                         from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import en                                   from '@angular/common/locales/en';
import { AbstractsComponent }               from '@views/abstracts/abstracts.component';
import { NzCardModule }                     from 'ng-zorro-antd/card';
import { PricesComponent }                  from '@views/prices/prices.component';
import { NzIconModule }                     from 'ng-zorro-antd';
import { RouterModule, Routes }             from '@angular/router';

registerLocaleData(en);

const VIEWS_COMPONENTS = [PricesComponent];
const BROWSER_MODULES = [CommonModule];
const ANT_DESIGN_MODULES = [NzCardModule, NzIconModule];

const routes: Routes = [
  {path: '', component: PricesComponent}
];

@NgModule({
  declarations: [VIEWS_COMPONENTS],
  imports: [BROWSER_MODULES, ANT_DESIGN_MODULES, RouterModule.forChild(routes)],
})
export class PricesModule {
}
