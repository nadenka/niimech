import { NgModule }             from '@angular/core';
import { CommonModule }         from '@angular/common';
import { FeedbackComponent }    from './feedback.component';
import { RouterModule, Routes } from '@angular/router';

const VIEWS_COMPONENTS = [FeedbackComponent];
const BROWSER_MODULES = [CommonModule];

const routes: Routes = [
  {path: '', component: FeedbackComponent}
];

@NgModule({
  declarations: [VIEWS_COMPONENTS],
  imports: [BROWSER_MODULES, RouterModule.forChild(routes)],
})
export class FeedbackModule { }
