import { Component }        from '@angular/core';
import { AppConfigService } from '../../app-config.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent {
  feedback = this.appConfigService.config.value['FEEDBACK']['FEEDBACK_ARR'];

  constructor(
    private appConfigService: AppConfigService
  ) {
  }
}
