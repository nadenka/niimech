import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AppConfigService }                   from '../../app-config.service';

@Component({
  selector: 'app-abstracts',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './abstracts.component.html',
  styleUrls: ['./abstracts.component.scss']
})
export class AbstractsComponent {
  abstracts = this.appConfigService.config.value['PROCEEDINGS']['ABSTRACT_LIST'];

  constructor(
    private appConfigService: AppConfigService
  ) {
  }
}
