import { NgModule }                         from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import en                                   from '@angular/common/locales/en';
import { AbstractsComponent }               from '@views/abstracts/abstracts.component';
import { NzCardModule }                     from 'ng-zorro-antd/card';
import { RouterModule, Routes }             from '@angular/router';

registerLocaleData(en);

const VIEWS_COMPONENTS = [AbstractsComponent];
const BROWSER_MODULES = [CommonModule];
const ANT_DESIGN_MODULES = [NzCardModule];


const routes: Routes = [
  {path: '', component: AbstractsComponent}
];

@NgModule({
  declarations: [VIEWS_COMPONENTS],
  imports: [BROWSER_MODULES, ANT_DESIGN_MODULES, RouterModule.forChild(routes) ],
})
export class AbstractsModule {
}
