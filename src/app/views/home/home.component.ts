import { ChangeDetectionStrategy, Component, DoCheck } from '@angular/core';
import { AppConfigService }                            from '../../app-config.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements DoCheck{
  array;
  homePageText;
  date;


  constructor(
    private appConfigService: AppConfigService,
  ) {
  this.getData();
  }

  getData() {
    if (!this.appConfigService.config.value) {
      setTimeout(() => (this.getData()), 1000);
    }
  }

  ngDoCheck(){
    const home = this.appConfigService.config.value['HOME'];
    this.array = home['HOME_PICTURES_LIST'] ;
    this.homePageText = home['HOME_PAGE_TEXT'];
    this.date = home['HOME_PAGE_DATE'];
  }
}
