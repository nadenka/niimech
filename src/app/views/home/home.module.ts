import { NgModule }         from '@angular/core';
import { CommonModule }     from '@angular/common';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { HomeComponent }    from './home.component';
import { RouterModule }     from '@angular/router';

const ANT_DESIGN_MODULES = [NzCarouselModule];

@NgModule({
  imports: [
    RouterModule.forChild([{path: '', component: HomeComponent}]),
    CommonModule,
    ANT_DESIGN_MODULES
  ],
  declarations: [HomeComponent]
})
export class HomeModule {
}
