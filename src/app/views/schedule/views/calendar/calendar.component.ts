import { ChangeDetectionStrategy, Component, HostListener, OnInit } from '@angular/core';
import { includes }                                                 from 'lodash';
import { AppConfigService }                                         from '../../../../app-config.service';

const hzDate = new Date();

@Component({
  selector: 'app-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  isMobile = false;
  selectedDate = hzDate;
  prevDay = hzDate;
  dates: Array<{ day: number, titles: Array<string> }> = [];
  mode: 'month' | 'year' = 'month';
  modalTitle: string;
  program: Array<string> | null = null;
  isVisible = false;
  days = [];
  includes = includes;
  listOfData = this.appConfigService.config.value['PROGRAM']['PROGRAM_TEXT'];

  @HostListener('window:resize', ['$event']) onResize() {
    this.setMenuMode(window.innerWidth);
  }

  constructor(
    private appConfigService: AppConfigService
  ) {
  }

  ngOnInit() {
    let uniqArr = [];
    this.listOfData.forEach(el => uniqArr = !includes(uniqArr, el.date) ? [...uniqArr, el.date] : [...uniqArr]);
    uniqArr.forEach(el => {
      const titleArr = this.listOfData.filter(x => x.date === el).map(y => `${y.time}: ${y.title};`);
      const year = +el.substr(6, 4);
      const month = +el.substr(3, 2) - 1;
      const date = +el.substr(0, 2);
      const day = +new Date(year, month, date);
      this.dates = [...this.dates, {day, titles: titleArr}];
      this.days = [...this.days, day];
    });
  }

  onClick() {
    if (this.selectedDate.getMonth() === this.prevDay.getMonth() && this.selectedDate.getFullYear() === this.prevDay.getFullYear()) {
      this.modalTitle = 'Program on day: ' + this.selectedDate.toString().slice(0, 15);
      this.isVisible = true;
      this.getDayProgram();
    }
    this.prevDay = this.selectedDate;
  }

  getDayProgram() {
    for (const el of this.dates) {
      if (el.day - +this.selectedDate === 0) {
        this.program = el.titles;
        break;
      } else {
        this.program = null;
      }
    }
  }


  setMenuMode(width) {
    if (width <= 576) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
  }
}
