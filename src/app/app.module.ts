import { BrowserModule }             from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { HttpClientModule }          from '@angular/common/http';
import { BrowserAnimationsModule }   from '@angular/platform-browser/animations';
import { registerLocaleData }        from '@angular/common';
import en                            from '@angular/common/locales/en';
import ru                            from '@angular/common/locales/ru';
import { HomeModule }                from '@views/home';
import { FullLayoutModule }          from '@containers/full-layout';
import { isRu }                      from '@constants/shared';
import { NZ_I18N, en_US, ru_RU }     from 'ng-zorro-antd/i18n';
import { AppRoutingModule }          from './app-routing.module';
import { AppComponent }              from './app.component';
import { AppConfigService }          from './app-config.service';

registerLocaleData(isRu ? ru : en);

const BROWSER_MODULES = [BrowserModule, BrowserAnimationsModule];

@NgModule({
  imports: [
    AppRoutingModule,
    FullLayoutModule,
    HomeModule,
    HttpClientModule,
    BROWSER_MODULES
  ],
  declarations: [AppComponent],
  providers: [
    {provide: NZ_I18N, useValue: isRu ? ru_RU : en_US},
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [AppConfigService],
      useFactory: (appConfigService: AppConfigService) => () => appConfigService.loadConfig()
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
