export const isRu = false;

export const PROGRAM_TITLE = {basic: 'Basic program', optional: 'Optional activities'};

export const PAGES_LIST = [
  {link: 'home', name: 'Home', icon: 'home'},
  {link: 'program', name: 'Program', icon: 'ordered-list'},
  {link: 'prices', name: 'Dates & Prices', icon: 'account-book'},
  {link: 'schedule', name: 'Schedule', icon: 'schedule'},
  {link: 'abstracts', name: 'Proceedings', icon: 'file'},
  {link: 'gallery', name: 'Gallery', icon: 'picture'},
  {link: 'about-us', name: 'About us', icon: 'user'},
  {link: 'feedback', name: 'Feedback', icon: 'container'},
  {link: 'contests', name: 'Student Сontests', icon: 'flag'},
];

// Данные для вкладок

export const  PAGES_ARR = {
  program: ['Basic', 'Cultural'],
  schedule: ['Schedule', 'Table']
};
