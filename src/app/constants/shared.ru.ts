export const isRu = true;

export const PROGRAM_TITLE = {basic: 'Основная программа', optional: 'Культурная программа'};

export const PAGES_LIST = [
  {link: 'home', name: 'Главная', icon: 'home'},
  {link: 'program', name: 'Программа', icon: 'ordered-list'},
  {link: 'prices', name: 'Даты и цены', icon: 'account-book'},
  {link: 'schedule', name: 'Расписание', icon: 'schedule'},
  {link: 'abstracts', name: 'Труды Летней Школы', icon: 'file'},
  {link: 'gallery', name: 'Галерея', icon: 'picture'},
  {link: 'about-us', name: 'О нас', icon: 'user'},
  {link: 'feedback', name: 'Отзывы', icon: 'container'},
  {link: 'contests', name: 'Студенческие Конкурсы', icon: 'flag'},
];

// Данные для вкладок

export const  PAGES_ARR = {
  program: ['Основная', 'Культурная'],
  schedule: ['Календарь', 'Таблица']
};

