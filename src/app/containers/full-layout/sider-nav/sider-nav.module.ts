import { NgModule }          from '@angular/core';
import { CommonModule }      from '@angular/common';
import { RouterModule }      from '@angular/router';
import { NzMenuModule }      from 'ng-zorro-antd/menu';
import { NzIconModule }      from 'ng-zorro-antd/icon';
import { SiderNavComponent } from './sider-nav.component';
import { NzToolTipModule }   from 'ng-zorro-antd/tooltip';

const ANT_DESIGN_MODULES = [NzMenuModule, NzIconModule, NzToolTipModule];

@NgModule({
  imports: [CommonModule, RouterModule, ANT_DESIGN_MODULES],
  declarations: [SiderNavComponent],
  exports: [SiderNavComponent]
})
export class SiderNavModule {
}
