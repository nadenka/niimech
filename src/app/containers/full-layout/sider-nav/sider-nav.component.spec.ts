import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SiderNavComponent }         from './sider-nav.component';

describe('SiderNavComponent', () => {

  let component: SiderNavComponent;
  let fixture: ComponentFixture<SiderNavComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SiderNavComponent]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiderNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
