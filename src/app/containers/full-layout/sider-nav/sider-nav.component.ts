import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-sider-nav',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './sider-nav.component.html',
  styleUrls: ['./sider-nav.component.scss']
})
export class SiderNavComponent {
  @Input() pagesList;
  @Input() isCollapsed: boolean;
}
