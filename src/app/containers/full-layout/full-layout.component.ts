import { ChangeDetectionStrategy, Component, HostListener } from '@angular/core';
import { PAGES_LIST }                                       from '@constants/shared';

@Component({
  selector: 'app-full-layout',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './full-layout.component.html',
  styleUrls: ['./full-layout.component.scss']
})
export class FullLayoutComponent {

  isMobile: boolean;
  isPad: boolean;
  isCollapsed: boolean;

  pagesList = PAGES_LIST;

  @HostListener('window:resize', ['$event']) onResize() {
    this.setMenuMode(window.innerWidth);
  }

  constructor() {
    this.setMenuMode(window.innerWidth);
  }

  setMenuMode(width) {
    if (width <= 576) {
      this.isMobile = true;
      this.isPad = false;
      this.isCollapsed = true;
    } else if (width > 576 && width < 922) {
      this.isMobile = false;
      this.isPad = true;
      this.isCollapsed = true;
    } else {
      this.isMobile = false;
      this.isPad = false;
      this.isCollapsed = false;
    }
  }
}
