import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { RouterModule }        from '@angular/router';
import { NzLayoutModule }      from 'ng-zorro-antd/layout';
import { FullLayoutComponent } from './full-layout.component';
import { SiderNavModule }      from './sider-nav';

const ANT_DESIGN_MODULES = [NzLayoutModule];

@NgModule({
  imports: [CommonModule, RouterModule, SiderNavModule, ANT_DESIGN_MODULES],
  declarations: [FullLayoutComponent]
})
export class FullLayoutModule {
}
