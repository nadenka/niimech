import { NgModule }                                from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { FullLayoutComponent }                     from '@containers/full-layout';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {
    path: '', component: FullLayoutComponent,
    children: [
      {path: 'home', loadChildren: () => import('./views/home/home.module').then(m => m.HomeModule)},
      {path: 'gallery', loadChildren: () => import('./views/gallery/gallery.module').then(m => m.GalleryModule)},
      {path: 'prices', loadChildren: () => import('./views/prices/prices.module').then(m => m.PricesModule)},
      {path: 'abstracts', loadChildren: () => import('./views/abstracts/abstracts.module').then(m => m.AbstractsModule)},
      {path: 'about-us', loadChildren: () => import('./views/about-us').then(m => m.AboutUsModule)},
      {path: 'program', loadChildren: () => import('./views/program').then(m => m.ProgramModule)},
      {path: 'schedule', loadChildren: () => import('./views/schedule').then(m => m.ScheduleModule)},
      {path: 'feedback', loadChildren: () => import('./views/feedback/feedback.module').then(m => m.FeedbackModule)},
      {path: 'contests', loadChildren: () => import('./views/contests/contests.module').then(m => m.ContestsModule)},
      {path: '*', redirectTo: ''},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
