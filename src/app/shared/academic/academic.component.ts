import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ExportService }                             from '@services/export';
import { AppConfigService }                          from '../../app-config.service';

@Component({
  selector: 'app-academic',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './academic.component.html',
  styleUrls: ['./academic.component.scss']
})
export class AcademicComponent {

  @Input() isTotal: boolean;

  listOfData = this.appConfigService.config.value['PROGRAM']['PROGRAM_TEXT'];

  constructor(
    private exportService: ExportService,
    private appConfigService: AppConfigService
  ) {
    this.listOfData.forEach(el => el['width'] = this.listOfData.filter(x => x.date === el.date).length);
  }

  export() {
    let exportArr = [];
    this.listOfData.forEach(el => {
      exportArr = [...exportArr, {
        date: el.date, time: el.time,
        lecturer: el.lecturer, title: el.title
      }];
    });
    this.exportService.exportExcel(exportArr, 'timetable');
  }
}
