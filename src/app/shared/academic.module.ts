import { NgModule }          from '@angular/core';
import { CommonModule }      from '@angular/common';
import { AcademicComponent } from './academic/academic.component';
import { NzTableModule }     from 'ng-zorro-antd/table';
import { NzButtonModule }    from 'ng-zorro-antd/button';
import { DateFormatPipe }    from './academic/date-format.pipe';
import { NzIconModule }      from 'ng-zorro-antd/icon';

const ANT_DESIGN_MODULES = [NzTableModule, NzButtonModule, NzIconModule];

@NgModule({
  imports: [
    CommonModule,
    ANT_DESIGN_MODULES
  ],
  declarations: [ AcademicComponent, DateFormatPipe],
  exports: [AcademicComponent]
})
export class AcademicModule {}
