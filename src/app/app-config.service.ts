import { Injectable }                  from '@angular/core';
import { HttpClient }                  from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { isRu }                  from '@constants/shared';

interface Config {
  HOST_API: string;
  HAS_AUTH: boolean;
  NO_TIPS: boolean;
  TIPS_LIST: string[];
}

@Injectable({providedIn: 'root'})
export class AppConfigService {

  private config$: Observable<Config> =   isRu ?  this.http.get<Config>('assets/app-config.ru.json') : this.http.get<Config>('assets/app-config.json');
  readonly config = new BehaviorSubject(null);

  constructor(
    private http: HttpClient,
  ) {
  }

  loadConfig = () => this.config$.subscribe((c: Config) => this.config.next(c));
}
